module SpectralMNIST

using MAT
using Statistics

export mnist_70k, mnist_300k, mnist_1m
export mnist_70k_labels, mnist_300k_labels, mnist_1m_labels
export centered_mnist_70k, centered_mnist_300k, centered_mnist_1m

center!(X) = (X .= X .- mean(X, dims = 2))
mnist_70k() = opendataset("70k")
mnist_300k() = opendataset("300k")
mnist_1m() = opendataset("1m")

mnist_70k_labels() = openlabels("70k")
mnist_300k_labels() = openlabels("300k")
mnist_1m_labels() = openlabels("1m")

centered_mnist_70k() = center!(mnist_70k())
centered_mnist_300k() = center!(mnist_300k())
centered_mnist_1m() = center!(mnist_1m())

opendataset(n) = read(matopen(path(n)), "X")
openlabels(n) = convert(Array{Int}, read(matopen(path(n)), "truth"))

path(v) = joinpath(pkgdir("SpectralMNIST"), "deps", "mnist_$(v)_spectral.mat")

pkgdir(pkgname) = dirname(dirname(Base.find_package(pkgname)))

end # module
